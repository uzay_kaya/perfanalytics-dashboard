import './App.css';
import Dashboard from "./components/Dashboard/Dashboard";
import { unstable_createMuiStrictModeTheme as createMuiTheme } from '@material-ui/core';
import { CssBaseline } from "@material-ui/core";
import { ThemeProvider } from "@material-ui/styles";

const theme = createMuiTheme({
  palette: {
    type: "dark"
  }
});

function App() {
  return (
    <>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Dashboard />
      </ThemeProvider>
    </>
  );
}

export default App;
