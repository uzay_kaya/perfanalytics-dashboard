## PerfAnalytics

Perfanalytics is an ecosystem which collects and criticizes web performance data. It consists three subsystems:

+ JS Library
+ API
+ Dashboard

> Dashboard application shows the last 30 minutes of TTFB, FCP, Dom Load, and Window Load as in charts.

### :heavy_check_mark: Status of Project
---
+ I think the project is almost complete. You can see a demonstration of the app [here](http://157.230.238.12:3004//). There are some work to do but it can provide main functionalities in the given acceptance criteria.
+ For API Project tests don't run before each deployment due to "Jest did not exit one second after the test run has completed. error". I will fix that issue in the future commits.

### :pushpin: Dashboard
---

UI side written with React and consists of 2 components. These are:
+ PerformanceChart - The component that makes up the chart structure. Used react-chartjs-js2 module.
+ Dashboard

<a href="https://resimyukle.xyz/i/7AI5Jy"><img src="https://i.resimyukle.xyz/7AI5Jy.png" /></a>

### :pushpin: Test Client application
---
[uzay.dev Project](https://gitlab.com/uzay_kaya/perfanalytics-client) A simple react project created with the create-react-app command
for send performance datas to dashboard with this body tag :

<a href="https://resimyukle.xyz/i/zT03J4"><img src="https://i.resimyukle.xyz/zT03J4.png" /></a>

### :pushpin: JS Library
---
[JS Project](https://gitlab.com/uzay_kaya/perfanalytics-js) is a client side library, which collects some performnace related key metrics from browser and sends to the Dashboard.

+ Used APIs
    + PerformanceObserver
    + Performance

### :whale: CI/CD
---
+ I decided to use Gitlab pipeline for this project. Gitlab provides a lot of functionality in one place so it seems to be a good solution for developers. There is no need a lot of configuration. It has it's own docker registry, codebase and environment variables etc. We need only a `gitlab-ci.yml`, `Dockerfile`, `nginx.conf`. All we have to do is configure this files and push our code to relevant branch.
+ I used DigitalOcean for hosting. 

### :round_pushpin: Development
---
+ In dev branch, for api; open [API Project](https://gitlab.com/uzay_kaya/perfanalytics-api) and run npm install and npm run.
+ In dev branch, for library; open [JS Project](https://gitlab.com/uzay_kaya/perfanalytics-js) and run npm install and npm run.
+ In dev branch, for test client application; open [uzay.dev Project](https://gitlab.com/uzay_kaya/perfanalytics-client) and run npm install and npm run.
+ In dev branch, for dashboard; open [Dashboard Project](https://gitlab.com/uzay_kaya/perfanalytics-dashboard) and run npm install and npm run. 
+ For test client application -> go to http://localhost:3000/
+ For dashboard --> go to http://localhost:3006/

### :round_pushpin: Production
---
+ For dashboard --> go to http://157.230.238.12:3004/
+ For test client application --> http://157.230.238.12:3005/
> You can refresh the page in the test client application to send data to the dashboard application.
> You can add the given script above to another page.
