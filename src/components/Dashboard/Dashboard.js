import React, { useState, useEffect } from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import {Backdrop, CircularProgress, Typography} from "@material-ui/core";
import axios from 'axios';
import PerformanceChart from "../PerformanceChart/PerformanceChart";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  chartCard: {
    height: "400px",
    margin: 20,
    justifyContent: "center",
    alignItems: "center",
    transition: "0.3s",
    boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 16px 60px -12.125px #808080",
    },
  },
  dashboardTitle: {
    padding: 10,
    marginTop: 10,
    fontSize: 25,
    fontWeight: "bold",
    textAlign: "center",
  },
  titleCard: {
    margin: 20,
    boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

const Dashboard = () => {
  const classes = useStyles();
  const [ performanceData, setPerformanceData ] = useState([]);
  const [ backdropOpen, setBackdropOpen ] = useState(true);

  const timeIntervalAsMinute = (30 * 60 * 1000);
  const currentTime = new Date().getTime();
  const beginTime = new Date(currentTime - timeIntervalAsMinute).getTime();
  const TTFB = [], FCP = [], WindowLoad = [], DomLoad = [];
  const createdAtTTFB = [], createdAtFCP = [], createdAtWindowLoad = [], createdAtDomLoad = [];

  useEffect(() => {
    async function getPerformanceData(){
      try {
        const response = await axios.get(`http://157.230.238.12:5000`, {
          params: {
            beginTime: beginTime,
            endTime: currentTime
          }
        });
        setPerformanceData(response.data)
        handleBackdropClose();
      } catch(error) {
        console.log("error", error);
      }
    }
    getPerformanceData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);


  const filterDataType = (data) => {
    if (data.analyticType === "TTFB") {
      TTFB.push(data.time);
      createdAtTTFB.push(new Date(data.createdAt).toLocaleDateString() + ' ' + new Date(data.createdAt).toLocaleTimeString());
    }
    else if (data.analyticType === "FCP") {
      FCP.push(data.time);
      createdAtFCP.push(new Date(data.createdAt).toLocaleDateString() + ' ' + new Date(data.createdAt).toLocaleTimeString());
    }
    else if (data.analyticType === "WINDOW_LOAD") {
      WindowLoad.push(data.time);
      createdAtWindowLoad.push(new Date(data.createdAt).toLocaleDateString() + ' ' + new Date(data.createdAt).toLocaleTimeString());
    }
    else if (data.analyticType === "DOM_LOAD") {
      DomLoad.push(data.time);
      createdAtDomLoad.push(new Date(data.createdAt).toLocaleDateString() + ' ' + new Date(data.createdAt).toLocaleTimeString());
    }
  }

  const handleBackdropClose = () => {
    setBackdropOpen(false);
  };

  performanceData.filter(filterDataType);
  console.log (performanceData)

  return (
    <div>
      <Grid container spacing={0} className={classes.root}>
        <Grid item xs={12}>
          <Card className={classes.titleCard}>
            <Typography
              className={classes.dashboardTitle}
              color="textSecondary"
              gutterBottom
            >
              Perfanalytics Dashboard
            </Typography>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={6}>
          <Card className={classes.chartCard}>
            <PerformanceChart data={TTFB} title={"TTFB"} labels={createdAtTTFB}/>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={6}>
          <Card className={classes.chartCard}>
            <PerformanceChart data={FCP} title={"FCP"} labels={createdAtFCP} />
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={6}>
          <Card className={classes.chartCard}>
            <PerformanceChart data={DomLoad} title={"DOM Load"} labels={createdAtDomLoad} />
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={6}>
          <Card className={classes.chartCard}>
            <PerformanceChart data={WindowLoad} title={"Window Load"} labels={createdAtWindowLoad} />
          </Card>
        </Grid>
      </Grid>
      <Backdrop className={classes.backdrop} open={backdropOpen} onClick={handleBackdropClose}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
};

export default Dashboard;
