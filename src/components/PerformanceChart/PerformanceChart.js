import React, { useState, useEffect } from 'react';
import { Line } from 'react-chartjs-2';
import "./PerformanceChart.css"

const PerformanceChart = (props) => {
  const [ data, setData ] = useState([]);
  const [ title, setTitle ] = useState('');
  const [ labels, setLabels ] = useState([]);

  useEffect(() => {
    setData(props.data);
    setTitle(props.title);
    setLabels(props.labels);
  }, [ props.data, props.title, props.labels ]); //ComponentDidMount


  const performanceData = {
    labels: labels,
    datasets: [
      {
        fill: false,
        lineTension: 0.2,
        backgroundColor: "rgba(75,192,192,0.2)",
        borderColor: "rgba(75,192,192,1)",
        borderWidth: 2,
        data: data,
      }
    ]
  }

  return (
    <div className="line-chart-container">
      <Line
        data={performanceData}
        options={{
          responsive: true,
          maintainAspectRatio: false,
          layout: {
            padding: {
              top: 5,
              left: 15,
              right: 15,
              bottom: 15
            }
          },
          title: {
            display: true,
            text: title,
            fontSize: 20,
          },
          legend: {
            display: false
          },
          tooltips: {
            displayColors: false,
            titleFontSize: 16,
            bodyFontSize: 14,
            xPadding: 10,
            yPadding: 10,
          }
        }}
      />
    </div>
  );
}

export default PerformanceChart;