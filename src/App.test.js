import App from './App';
import { shallow } from "enzyme";
import Dashboard from "./components/Dashboard/Dashboard";

describe('<App />', () => {
  let component;

  beforeEach(() => {
    component = shallow(<App/>)
  });

  it("App renders without crashing", () => {
    expect(component.exists()).toEqual(true);
  });

  it('Dashboard renders without crashing', () => {
    expect(component.find(Dashboard)).toHaveLength(1);
  });
});
