import { shallow } from "enzyme";
import PerformanceChart from "../../PerformanceChart/PerformanceChart";
import {Line} from "react-chartjs-2";

const data = [];
const title = "";
const labels = [];
const label = "";

describe('<PerformanceChart />', () => {
  let component;
  let props = { data, title, labels, label }

  beforeEach(() => {
    component = shallow(<PerformanceChart {...props}/>)
  });

  it('PerformanceChart component render without crashing', () => {
    expect(component.exists()).toEqual(true);
  });

  it('Line component renders without crashing', () => {
    expect(component.find(Line)).toHaveLength(1);
  });
});