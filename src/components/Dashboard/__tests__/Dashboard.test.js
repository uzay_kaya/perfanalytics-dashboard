import { shallow } from "enzyme";
import Dashboard from "../Dashboard";
import PerformanceChart from "../../PerformanceChart/PerformanceChart";
import { act } from 'react-dom/test-utils';
import axios from "axios";

describe('<Dashboard />', () => {
  let component;

  beforeEach(() => {
    component = shallow(<Dashboard/>)
  });

  it("Dashboard renders without crashing", () => {
    expect(component.exists()).toEqual(true);
  });

  it('PerformanceChart components render without crashing', () => {
    expect(component.find(PerformanceChart)).toHaveLength(4);
  });
});
